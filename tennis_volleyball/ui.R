library(shiny)
library(shinydashboardPlus)
library(shinydashboard)
library(shinyalert)
library(data.table)
library(dplyr)
library(shinyWidgets)
library(lubridate)
library(filesstrings)
library(RMySQL)
library(geoloc)
library(leaflet)
library(shinyBS)

###############################################################################
IsThereNewFile <- function(){ 
  
  filenames <- list.files(path = 'Data/',pattern="*.csv", full.names=TRUE)
  length(filenames)
}

ReadAllData=function(){ 
  
  filenames <- list.files(path = 'Data/',pattern="*.csv", full.names=TRUE)
  temp= rbindlist(lapply(filenames, fread))
  # temp = if(nrow(temp) < 1){data.frame(NA,NA,NA,NA)}else{temp}
  
  return(temp)
  
}

hav.dist <- function(long1, lat1, long2, lat2) {
  R <- 6371
  diff.long <- (long2 - long1)
  diff.lat <- (lat2 - lat1)
  a <- sin(diff.lat/2)^2 + cos(lat1) * cos(lat2) * sin(diff.long/2)^2
  b <- 2 * asin(pmin(1, sqrt(a))) 
  d = R * b
  return(d)
}

#################################################################################


#pull data-----------------
#read icons
# tennis_icons = makeIcon(
#     iconUrl = "https://img.icons8.com/dusk/64/000000/tennis.png",
#     iconWidth = 18, iconHeight = 18,
#     iconAnchorX = 18, iconAnchorY = 18
# )

myicons = iconList(
  court = makeIcon(iconUrl = "https://img.icons8.com/dusk/64/000000/tennis.png"
                   ,iconWidth = 18, iconHeight = 18
                   ,iconAnchorX = 18, iconAnchorY = 18),
  me = makeIcon("https://img.icons8.com/dusk/64/000000/street-view.png"
                ,iconWidth = 20, iconHeight = 20
                ,iconAnchorX = 20, iconAnchorY = 20)
)


#create a mock table
mydb = dbConnect(MySQL(), user='faukita',
                 password='greenappl3',
                 host='testmysqlfn.cggxxglafgsh.us-west-2.rds.amazonaws.com',
                 dbname = 'masterlev')


# Define UI for application that draws a histogram
ui = fluidPage(
  navbarPage(
    "Tennis App",
             id = 'tabs',
             tabPanel("explore", useShinydashboardPlus()
                      #,tags$head(includeCSS("styles.css"))
                      ,fluidRow(
                        column(9,offset = 1,h3(htmlOutput("Tennis App")))),
                      
                      column(12,offset = 3,    boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = '', status = 'success',
                                                       actionButton("map","get map"),
                                                       actionButton("filters", "add filters"))
                             ),
                      column(12,offset = 3,
                             boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = 'Courts Near Me', status = 'success',
                                     uiOutput("myboxes"),
                                     textOutput("print"),bsModal("mod","title","btn")
                             )
                             )
                      ),
             tabPanel("favorites", 
                      fluidRow(column(9,offset = 1,h3(htmlOutput("datameetup1")))),
                      column(12,offset = 3,
                             boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = 'My Favorite Courts', status = 'success',
                                     shinydashboard::valueBoxOutput('f1', width = 12)
                                     )
                             ),
                      column(12,offset = 3,
                             boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = 'My Favorite Partners', status = 'success',
                                     shinydashboard::valueBoxOutput('f2', width = 12)
                             )
                      )
                             
                      
                      ),
             tabPanel("partners",
                      #fluidRow(
                      #  column(9,offset = 1,h3(htmlOutput("Tennis App")))),
                      
                      column(12,offset = 3,    boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = 'Partners Near Me', status = 'success',
                                                       actionButton("find_ppl", "Find Partners Near Me"))
                      ),
                      column(12,offset = 3,
                             boxPlus(solidHeader = T, collapsible = F, collapsed = F, closable = F, title = '', status = 'success',
                                     uiOutput("myPartners")#,
                                     #textOutput("print"),bsModal("mod","title","btn")
                             )
                      )
                      
                      ),
             tabPanel("map",
                      #fluidRow(
                        geoloc::onload_geoloc(),
                        leafletOutput("lf")#, height = '10%')
                      ),
    tabPanel("settings",
             #fluidRow(
             actionButton("change_radius", "Change Search Radius for Partners and Courts"),
             actionButton("do_something", "Change Something Else")
    )
             )
             #)
  , style='height: 1100px')

